%global __provides_exclude_from ^%{python3_sitearch}/.*\\.so$
%global optflags %(echo %{optflags} -I%{_includedir}/libev)

Name:          python-gevent
Version:       22.10.2
Release:       2
Summary:       A coroutine-based Python networking library
License:       MIT
URL:           http://www.gevent.org/
Source0:       %{pypi_source gevent}
# https://github.com/gevent/gevent/commit/6b22af0fa8eb2efa89fce36c35808948c67352b0
Patch0:        CVE-2023-41419-pre.patch
# https://github.com/gevent/gevent/commit/2f53c851eaf926767fbac62385615efd4886221c
Patch1:        CVE-2023-41419.patch

BuildRequires: gcc c-ares-devel libev-devel libuv-devel

%description
gevent is a coroutine -based Python networking library that uses greenlet to
provide a high-level synchronous API on top of the libev or libuv event loop.

Features include:
  * Fast event loop based on libev or libuv.
  * Lightweight execution units based on greenlets.
  * API that re-uses concepts from the Python standard library (for examples there are events and queues).
  * Cooperative sockets with SSL support
  * Cooperative DNS queries performed through a threadpool, dnspython, or c-ares.
  * Monkey patching utility to get 3rd party modules to become cooperative
  * TCP/UDP/HTTP servers
  * Subprocess support (through gevent.subprocess)
  * Thread pools

%package_help

%package -n python3-gevent
Summary:       %{summary}
%{?python_provide:%python_provide python3-gevent}
BuildRequires: python3-devel python3-greenlet-devel
BuildRequires: python3-setuptools python3-Cython
Requires:      python3-greenlet

%description -n python3-gevent
gevent is a coroutine -based Python networking library that uses greenlet to
provide a high-level synchronous API on top of the libev or libuv event loop.

Features include:
  * Fast event loop based on libev or libuv.
  * Lightweight execution units based on greenlets.
  * API that re-uses concepts from the Python standard library (for examples there are events and queues).
  * Cooperative sockets with SSL support
  * Cooperative DNS queries performed through a threadpool, dnspython, or c-ares.
  * Monkey patching utility to get 3rd party modules to become cooperative
  * TCP/UDP/HTTP servers
  * Subprocess support (through gevent.subprocess)
  * Thread pools

Python 3 version.

%prep
%autosetup -n gevent-%{version} -p1
# Remove bundled libraries
rm -r deps
# C extension sources will go into debuginfo as normal.
sed -i -e 's/include_package_data=True/include_package_data=False/' setup.py

%build
export LIBEV_EMBED=0
export CARES_EMBED=0
export GEVENT_NO_CFFI_BUILD=1
%py3_build

%install
export LIBEV_EMBED=0
export CARES_EMBED=0
export GEVENT_NO_CFFI_BUILD=1
%py3_install
rm %{buildroot}%{python3_sitearch}/gevent/_*2.py
rm %{buildroot}%{python3_sitearch}/gevent/__pycache__/_*2.*
find %{buildroot} -name '.buildinfo' -delete
# Correct the permissions.
find %{buildroot} -name '*.so' -exec chmod 755 {} ';'

%files help
%doc README.rst

%files -n python3-gevent
%defattr(-,root,root)
%license LICENSE
%{python3_sitearch}/gevent*

%changelog
* Tue Sep 26 2023 yaoxin <yao_xin001@hoperun.com> - 22.10.2-2
- Fix CVE-2023-41419

* Thu Jun 01 2023 Ge Wang <wang__ge@126.com> - 22.10.2-1
- Update to version 22.10.2

* Fri May 06 2022 wulei <wulei80@h-partners.com> - 21.1.2-2
- Fix cannot import name _corecffi from gevent.libuv

* Wed Mar 30 2022 caodongxia <caodongxia@huawei.com> - 21.1.2-1
- Update to 21.1.2

* Wed Oct 21 2020 leiju <leiju4@huawei.com> - 1.3.6-3
- remove python2 subpackage

* Tue Dec 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.6-2
- modify spec format

* Tue Dec 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.6-1
- Package init
